import subprocess
import os

subprocess.run(['meson', '--prefix=/tmp/simple', 'build'])
os.chdir('build')
subprocess.run(['ninja'])
subprocess.run(['ninja',  'install'])