import itertools
import json
import tempfile

from gi.repository import Gtk, Gdk

from .models import Shape, ShapeGroup, ShapeType, Color, Settings, ClickMode, Canvas, CommandStack
from .commands import MoveCommand, DrawCommand, ResizeCommand, GroupCommand, CaptionCommand
from .visitors import ToJsonVisitor

_MINIMAL_SIZE = 10


def _brighten_color(color):
    return Color(*(_brighten_shade(shade) for shade in color))


def _brighten_shade(color):
    return color + ((1 - color) / 2)

def _save_file(canvas):
    visitor = ToJsonVisitor()
    data = canvas.accept(visitor)
    with open(tempfile.gettempdir() + '/simpledrawingarea.json', 'w') as fp:
        fp.write(json.dumps(data, indent=True))

def _load_file():
    try:
        with open(tempfile.gettempdir() + '/simpledrawingarea.json') as fp:
            Settings().commands = CommandStack()
            return Canvas().from_json(json.load(fp))
    except IOError:
        print('oops')
        return Canvas()


class SimpleDrawingArea(Gtk.DrawingArea):
    __gtype_name__ = 'SimpleDrawingArea'

    def __init__(self):
        super().__init__()

        self._last_point = None
        self._anchor_point = None
        self._last_temp_shape = None
        self.canvas = Canvas()
        self.click_mode_at_click = None
        self.selected_shape = None
        self.grouped_shapes = set()

        self.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        self.add_events(Gdk.EventMask.BUTTON_RELEASE_MASK)
        self.add_events(Gdk.EventMask.KEY_PRESS_MASK)
        self.add_events(Gdk.EventMask.KEY_RELEASE_MASK)
        self.add_events(Gdk.EventMask.POINTER_MOTION_MASK)

        self.connect('draw', self._draw_callback)
        self.connect('button-press-event', self._press_callback)
        self.connect('button-release-event', self._release_callback)
        self.connect('key-press-event', self._key_press_callback)
        self.connect('key-release-event', self._key_release_callback)
        self.connect('motion-notify-event', self._motion_callback)

    def _key_press_callback(self, _, event):
        key = event.get_keyval().keyval
        modifiers = event.get_state()

        if key == 122 and modifiers & Gdk.ModifierType.CONTROL_MASK:
            Settings().commands.undo()
            self.queue_draw()
            return
        elif key == 90 and modifiers & Gdk.ModifierType.CONTROL_MASK:
            Settings().commands.redo()
            self.queue_draw()
            return
        elif key == 115 and modifiers & Gdk.ModifierType.CONTROL_MASK:
            _save_file(self.canvas)
            return
        elif key == 114 and modifiers & Gdk.ModifierType.CONTROL_MASK:
            self.grouped_shapes = set()
            self.canvas = _load_file()
            self.queue_draw()
            return
        elif key == 103 and modifiers & Gdk.ModifierType.CONTROL_MASK:
            command = GroupCommand(self.grouped_shapes, self.canvas)
            Settings().commands.push(command)
            self.grouped_shapes = set()
            self.queue_draw()
            return

        if self._last_temp_shape is not None:
            if key == 65507:
                self._last_temp_shape.shape_type = ShapeType.ELLIPSE
            else:
                self._last_temp_shape.shape_type = ShapeType.RECTANGLE
            self.queue_draw()
            return

        if len(self.grouped_shapes) == 1:
            if key == 65361:
                Settings().captions_position = 'left'
                return
            elif key == 65362:
                Settings().captions_position = 'top'
                return
            elif key == 65363:
                Settings().captions_position = 'right'
                return
            elif key == 65364:
                Settings().captions_position = 'bottom'
                return

            unicode_int = Gdk.keyval_to_unicode(key)
            if unicode_int:
                command = CaptionCommand(next(iter(self.grouped_shapes)), unicode_int, Settings().captions_position)
                Settings().commands.push(command)

    def _key_release_callback(self, _, event):
        key = event.get_keyval().keyval

        if self._last_temp_shape is not None:
            if key == 65507:
                self._last_temp_shape.shape_type = ShapeType.RECTANGLE

            self.queue_draw()

    def _motion_callback(self, _, event):
        coords = event.get_coords()
        modifiers = event.get_state()

        if self._last_point is not None and modifiers & Gdk.ModifierType.BUTTON1_MASK:
            if Gdk.ModifierType.CONTROL_MASK & modifiers:
                shape_type = ShapeType.ELLIPSE
            else:
                shape_type = ShapeType.RECTANGLE

            x = min(self._last_point[0], coords[0])
            y = min(self._last_point[1], coords[1])
            width = abs(self._last_point[0] - coords[0])
            height = abs(self._last_point[1] - coords[1])

            if width != 0 and height != 0:
                if self._last_temp_shape is None:
                    self._last_temp_shape = Shape(x, y, width, height, shape_type)
                else:
                    # TODO: Implement proper resize function for this shit
                    self._last_temp_shape.x = x
                    self._last_temp_shape.y = y
                    self._last_temp_shape.width = width
                    self._last_temp_shape.height = height

        self.queue_draw()

    def _press_callback(self, _, event):
        modifiers = event.get_state()
        if Gdk.ModifierType.SHIFT_MASK & modifiers:
            Settings().click_mode = ClickMode.EDIT
        elif Gdk.ModifierType.MOD1_MASK & modifiers:
            Settings().click_mode = ClickMode.SELECT
        else:
            Settings().click_mode = ClickMode.DRAW

        self.click_mode_at_click = Settings().click_mode

        if self.click_mode_at_click == ClickMode.DRAW:
            if event.get_button().button == 1:
                self._last_point = event.get_coords()
            elif event.get_button().button == 3:
                self._anchor_point = event.get_coords()
                shape = self.canvas.shape_at(self._anchor_point)
                if shape:
                    if shape in self.grouped_shapes:
                        self.grouped_shapes.remove(shape)
                    else:
                        self.grouped_shapes.add(shape)
        elif self.click_mode_at_click == ClickMode.EDIT or self.click_mode_at_click == ClickMode.SELECT:
            if event.get_button().button == 1:
                self._anchor_point = event.get_coords()
                self.selected_shape = self.canvas.shape_at(self._anchor_point)
                return

        self.selected_shape = None

    def _release_callback(self, _, event):
        coords = event.get_coords()

        if self.click_mode_at_click == ClickMode.DRAW:
            if (self._last_point is not None and event.get_button().button == 1 and self._last_temp_shape is not None and
                self._last_temp_shape.width != 0 and self._last_temp_shape.height != 0):
                self._last_temp_shape.width = max(_MINIMAL_SIZE, self._last_temp_shape.width)
                self._last_temp_shape.height = max(_MINIMAL_SIZE, self._last_temp_shape.height)

                command = DrawCommand(self._last_temp_shape, self.canvas)
                Settings().commands.push(command)

            self._last_point = None
            self._last_temp_shape = None
        elif self.click_mode_at_click == ClickMode.EDIT:
            if self.selected_shape and self._anchor_point:
                if self._anchor_point[0] < self.selected_shape.x + (self.selected_shape.width / 2):
                    changed_width = self.selected_shape.width - coords[0] + self._anchor_point[0]
                    new_width = max(_MINIMAL_SIZE, abs(changed_width))
                    if changed_width < 0:
                        x = self.selected_shape.x + self.selected_shape.width
                    else:
                        x = self.selected_shape.x + self.selected_shape.width - new_width
                else:
                    changed_width = self.selected_shape.width + coords[0] - self._anchor_point[0]
                    new_width = max(_MINIMAL_SIZE, abs(changed_width))
                    if changed_width < 0:
                        x = self.selected_shape.x - new_width
                    else:
                        x = self.selected_shape.x
                width = new_width

                if self._anchor_point[1] < self.selected_shape.y + (self.selected_shape.height / 2):
                    changed_height = self.selected_shape.height - coords[1] + self._anchor_point[1]
                    new_height = max(_MINIMAL_SIZE, abs(changed_height))
                    if changed_height < 0:
                        y = self.selected_shape.y + self.selected_shape.height
                    else:
                        y = self.selected_shape.y + self.selected_shape.height - new_height
                else:
                    changed_height = self.selected_shape.height + coords[1] - self._anchor_point[1]
                    new_height = max(_MINIMAL_SIZE, abs(changed_height))
                    if changed_height < 0:
                        y = self.selected_shape.y - new_height
                    else:
                        y = self.selected_shape.y
                height = new_height

                command = ResizeCommand(self.selected_shape, [width, height], [x, y])
                Settings().commands.push(command)
        elif self.click_mode_at_click == ClickMode.SELECT:
            if self.selected_shape and self._anchor_point:
                x =  self.selected_shape.x + coords[0] - self._anchor_point[0]
                y = self.selected_shape.y + coords[1] - self._anchor_point[1]
                command = MoveCommand(self.selected_shape, [x, y])
                Settings().commands.push(command)

        self._anchor_point = None
        self.selected_shape = None
        self.click_mode_at_click = None
        self.queue_draw()

    def _draw_callback(self, _, cr):
        width, height = self.get_allocated_width(), self.get_allocated_height()

        cr.set_source_rgb(1, 1, 1)
        cr.rectangle(0, 0, width, height)
        cr.fill()

        for shape in itertools.chain(self.canvas, [self._last_temp_shape]):
            selected = False

            if shape is None:
                continue
            if shape in self.grouped_shapes:
                selected = True
            _draw_shape(cr, shape, selected)

        for shape in self.canvas:
            # TODO
            pass


def _draw_shape(cr, shape, selected=False):
    if isinstance(shape, Shape):
        cr.save()
        shape.draw_strat(cr)
        cr.restore()

        cr.save()
        cr.set_line_width(4)
        if selected:
            cr.set_source_rgb(0, 0, 1)
            cr.stroke_preserve()
            cr.set_source_rgb(*_brighten_color(Color(0, 0, 1)))
        else:
            cr.set_source_rgb(*shape.color)
            cr.stroke_preserve()
            cr.set_source_rgb(*_brighten_color(shape.color))
        cr.fill()
        cr.restore()
    elif isinstance(shape, ShapeGroup):
        for item in shape.shapes:
            _draw_shape(cr, item, selected)