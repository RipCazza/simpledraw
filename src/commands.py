from .models import Canvas, Shape, ShapeGroup
from .visitors import MoveVisitor, ResizeVisitor


class MoveCommand():
    def __init__(self, shape, destination):
        super().__init__()
        self.shape = shape
        self.destination = destination
        self.origin = [shape.x, shape.y]

    def redo(self):
        visitor = MoveVisitor(self.destination)
        self.shape.accept(visitor)

    def undo(self):
        visitor = MoveVisitor(self.origin)
        self.shape.accept(visitor)


class DrawCommand():
    def __init__(self, shape, canvas):
        super().__init__()
        self.shape = shape
        self.canvas = canvas

    def redo(self):
        self.canvas.append(self.shape)

    def undo(self):
        self.canvas.remove(self.shape)


class ResizeCommand():
    def __init__(self, shape, new_size, destination=None):
        super().__init__()
        self.shape = shape
        self.new_size = new_size
        self.old_size = [shape.width, shape.height]
        self.origin = [shape.x, shape.y]
        if destination is not None:
            self.destination = destination
        else:
            self.destination = self.origin

    def redo(self):
        visitor = ResizeVisitor(self.new_size)
        self.shape.accept(visitor)

        visitor = MoveVisitor(self.destination)
        self.shape.accept(visitor)

    def undo(self):
        visitor = ResizeVisitor(self.old_size)
        self.shape.accept(visitor)

        visitor = MoveVisitor(self.origin)
        self.shape.accept(visitor)

class GroupCommand():
    def __init__(self, shapes, canvas):
        super().__init__()
        self.shapes = shapes
        self.canvas = canvas
        self.group = ShapeGroup()
        for shape in self.shapes:
            self.group.add(shape)

    def redo(self):
        for shape in self.group.shapes:
            self.canvas.remove(shape)
        self.canvas.append(self.group)

    def undo(self):
        for shape in self.shapes:
            self.canvas.append(shape)
        self.canvas.remove(self.group)

class CaptionCommand():
    def __init__(self, shape, unicode_int, captions_position):
        super().__init__()
        self.shape = shape
        self.unicode_int = unicode_int
        self.captions_position = captions_position
        self.old_text = self.shape.captions.get(self.captions_position)

    def redo(self):
        text = self.old_text
        if text is not None:
            if self.unicode_int == 8:
                text = text[:-1]
            else:
                text += chr(self.unicode_int)
        elif self.unicode_int != 8:
            text = chr(self.unicode_int)

        if text is None or len(text) == 0:
            self.shape.captions.pop(self.captions_position, None)
        else:
            self.shape.captions[self.captions_position] = text
        print(self.shape.captions)

    def undo(self):
        text = self.old_text
        if text is None or len(text) == 0:
            self.shape.captions.pop(self.captions_position, None)
        else:
            self.shape.captions[self.captions_position] = text
        print(self.shape.captions)