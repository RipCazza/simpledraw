import math
import random
from collections import namedtuple
from enum import Enum


Color = namedtuple('Color', ['r', 'g', 'b'])


def item_to_object(item):
    print(item)
    if item.get('group') is not None:
        return ShapeGroup.from_json(item['group'])
    elif item.get('shape') is not None:
        return Shape.from_json(item['shape'])


class ClickMode(Enum):
    DRAW = 1
    SELECT = 2
    EDIT = 3


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]


class Settings(metaclass=Singleton):

    def __init__(self):
        self.click_mode = ClickMode.DRAW
        self.captions_position = 'top'
        self.commands = CommandStack()


class CommandStack:

    def __init__(self):
        self._index_pointer = -1
        self._stack = []

    def push(self, command):
        del self._stack[self._index_pointer + 1:len(self._stack)]
        command.redo()
        self._stack.append(command)
        self._index_pointer = len(self._stack) - 1

    def redo(self):
        if self._index_pointer + 1 < len(self._stack):
            self._stack[self._index_pointer + 1].redo()
            self._index_pointer += 1

    def undo(self):
        if self._index_pointer >= 0:
            self._stack[self._index_pointer].undo()
            self._index_pointer -= 1

class Canvas(list):

    def __init__(self):
        pass

    @classmethod
    def from_json(cls, list):
        canvas = cls()
        for item in list:
            canvas.append(item_to_object(item))
        return canvas

    def accept(self, visitor, **kwargs):
        return visitor.visitCanvas(self, **kwargs)

    def shape_at(self, point):
        for shape in reversed(self):
            if shape.contains(point):
                return shape
        return None


class ShapeType(Enum):
    ELLIPSE = 'ellipse'
    RECTANGLE = 'rectangle'


def _draw_ellipse(cr, shape):
    cr.translate(shape.x + shape.width / 2, shape.y + shape.height / 2)
    cr.scale(shape.width / 2, shape.height / 2)
    cr.arc(0, 0, 1, 0, 2 * math.pi)


def _draw_rectangle(cr, shape):
    cr.rectangle(shape.x, shape.y, shape.width, shape.height)


class Shape:

    def __init__(self, x, y, width, height, shape_type, color=None, captions=None):
        self.x = x
        self.y = y
        self.height = height
        self.width = width
        self.shape_type = shape_type
        self.captions = dict()
        if color is not None:
            self.color = color
        else:
            self.color = Color(random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1))

        if captions is not None:
            self.captions = captions
        else:
            self.captions = dict()

    @classmethod
    def from_json(cls, dict):
        shape = cls(
            dict['x'],
            dict['y'],
            dict['width'],
            dict['height'],
            ShapeType(dict['shape_type']),
            dict['color'],
            dict['captions']
        )
        return shape

    def draw_strat(self, cr):
        return self._draw_strat(cr, self)

    def accept(self, visitor, **kwargs):
        return visitor.visitShape(self, **kwargs)

    def contains(self, point):
        if (self.x < point[0] and self.x + self.width > point[0] and
            self.y < point[1] and self.y + self.height > point[1]):
            return True
        return False

    @property
    def shape_type(self):
        return self._shape_type

    @shape_type.setter
    def shape_type(self, value):
        self._shape_type = value
        if self._shape_type == ShapeType.ELLIPSE:
            self._draw_strat = _draw_ellipse
        elif self._shape_type == ShapeType.RECTANGLE:
            self._draw_strat = _draw_rectangle


class ShapeGroup:

    def __init__(self, captions=None):
        self.x = None
        self.y = None
        self.height = None
        self.width = None
        self.shapes = []
        if captions is not None:
            self.captions = captions
        else:
            self.captions = dict()

    @classmethod
    def from_json(cls, dict):
        group = cls(
            dict['captions']
        )
        for item in dict['shapes']:
            group.add(item_to_object(item))
        return group

    def accept(self, visitor, **kwargs):
        return visitor.visitShapeGroup(self, **kwargs)

    def contains(self, point):
        for shape in self.shapes:
            if shape.contains(point):
                return True
        return False

    def add(self, shape):
        self.shapes.append(shape)
        self._fit_shapes()

    def remove(self, shape):
        self.shapes.remove(shape)
        self._fit_shapes()

    def _fit_shapes(self):
        left = None
        right = None
        top = None
        bottom = None

        for shape in self.shapes:
            if left is None or shape.x < left:
                left = shape.x
            if right is None or shape.x + shape.width > right:
                right = shape.x + shape.width
            if top is None or shape.y < top:
                top = shape.y
            if bottom is None or shape.y + shape.height > bottom:
                bottom = shape.y + shape.height

        self.x = left
        self.width = right - left
        self.y = top
        self.height = bottom - top
