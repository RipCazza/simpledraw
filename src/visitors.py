from abc import ABCMeta, abstractmethod


class Visitor(metaclass=ABCMeta):

    @abstractmethod
    def visitCanvas(self, canvas, **kwargs):
        pass

    @abstractmethod
    def visitShapeGroup(self, group, **kwargs):
        pass

    @abstractmethod
    def visitShape(self, shape, **kwargs):
        pass


class MoveVisitor(Visitor):

    def __init__(self, point):
        super().__init__()
        self.point = point

    def visitCanvas(self, canvas, **kwargs):
        pass

    def visitShapeGroup(self, group, **kwargs):
        point = kwargs.get('point', self.point)
        for shape in group.shapes:
            shape.accept(self, point=[
                shape.x + point[0] - group.x,
                shape.y + point[1] - group.y,
            ])
        self.visitShape(group, point=point)
        group._fit_shapes()

    def visitShape(self, shape, **kwargs):
        point = kwargs.get('point', self.point)
        shape.x = point[0]
        shape.y = point[1]

class ResizeVisitor(Visitor):

    def __init__(self, size):
        super().__init__()
        self.size = size

    def visitCanvas(self, canvas, **kwargs):
        pass

    def visitShapeGroup(self, group, **kwargs):
        size = kwargs.get('size', self.size)
        width_modifier = size[0] / group.width
        height_modifier = size[1] / group.height

        for shape in group.shapes:
            shape.accept(self, size=[
                shape.width * width_modifier,
                shape.height * height_modifier
            ])

            visitor = MoveVisitor([
                group.x + (shape.x - group.x) * width_modifier,
                group.y + (shape.y - group.y) * height_modifier,
            ])
            shape.accept(visitor)
        self.visitShape(group, size=size)
        group._fit_shapes()

    def visitShape(self, shape, **kwargs):
        size = kwargs.get('size', self.size)
        shape.width = size[0]
        shape.height = size[1]

class ToJsonVisitor(Visitor):

    def __init__(self):
        super().__init__()

    def visitCanvas(self, canvas, **kwargs):
        data = []
        for shape in canvas:
            data.append(shape.accept(self))
        return data

    def visitShapeGroup(self, group, **kwargs):
        data = []
        for shape in group.shapes:
            data.append(shape.accept(self))
        return {
            'group': {
                'shapes': data,
                'captions': group.captions
            }
        }

    def visitShape(self, shape, **kwargs):
        return {
            'shape': {
                'shape_type': shape.shape_type.value,
                'x': shape.x,
                'y': shape.y,
                'width': shape.width,
                'height': shape.height,
                'color': shape.color,
                'captions': shape.captions
            }
        }
